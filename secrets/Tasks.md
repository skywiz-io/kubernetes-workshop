- Create secret
`kubectl create secret generic db-user-pass --from-literal=username=admin --from-literal=password=mypassword`

- Inspect secret
`kubectl describe secrets db-user-pass `

- Get secret as yaml
` kubectl get secrets db-user-pass -o yaml `

- Decode secret
`echo <String-from-above-yaml> | base64 --decode`

- Create secret-pod-volume.yaml  with the secret as volume
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx
    volumeMounts:
    - name: dbcred
      mountPath: "/mnt/db-user-pass"
      readOnly: true
  volumes:
  - name: dbcred
    secret:
      secretName: db-user-pass
```
- Create pod with secret as volume
`kubectl apply -f secret-pod-volume.yaml`

- login to pod
`kubectl exec -it nginx -- /bin/bash`

- Check secret mount as volume
`cat /mnt/db-user-pass/password`
`cat /mnt/db-user-pass/username`

- Exit from pod and delete pod
`exit`
`kubectl delete -f secret-pod-volume.yaml`


- Create secret-pod-env.yaml with below code
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx
    env:
      - name: SECRET_USERNAME
        valueFrom:
          secretKeyRef:
            name: db-user-pass
            key: username
      - name: SECRET_PASSWORD
        valueFrom:
          secretKeyRef:
            name: db-user-pass
            key: password
  restartPolicy: Never
```
- Create pod with secret as environment
`kubectl apply -f secret-pod-env.yaml `

- login to pod
`kubectl exec -it nginx -- /bin/bash`

- Check secret mount as environment
`echo $SECRET_USERNAME`
`echo $SECRET_PASSWORD`

- Exit from pod and delete pod
`exit`
`kubectl delete -f secret-pod-env.yaml`

- Delete secret
`kubectl delete secret db-user-pass `

