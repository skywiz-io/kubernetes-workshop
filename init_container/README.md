- Create a pod with 1 container and init container (nginx and busybox)

- Share storage resource - volume (empty directory) between them

- Make init container to write to the shared resource (busybox)

- Read the file from the second pod (nginx)


Example for init container:
```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo The app is running! && sleep 3600']
  initContainers:
  - name: init-myservice
    image: busybox:1.28
    command: ['sh', '-c', 'until nslookup myservice; do echo waiting for myservice; sleep 2; done;']
```

https://kubernetes.io/docs/concepts/workloads/pods/init-containers/
