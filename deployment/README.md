# Deployment exercise


## Part 1 

- Create deployment  
`kubectl create deployment webapp --image=nginx`


- Get pods  
`kubectl get pods -o wide  `

- Inspect deployment  
`kubectl describe deployments webapp `

- Scale deployment  
`kubectl scale deployment webapp --replicas=3 `

- Get pods  
`# kubectl get pods -o wide `

## Part 2

- Inspect deployments  
`kubectl describe deployments webapp `

- Get containter name from deployment  
`kubectl get deployments webapp -o yaml `

- Setup nginx containter image to version 1.9.1  
`kubectl  set image deployment/<deployment name> <Containter-name>=nginx:1.9.1 `

- Check rollout status  
`kubectl rollout status deployment <deployment name> `

- Inspect deployments  
`kubectl describe deployments <deployment name> `

- Check deployment history  
`kubectl rollout history deployment <deployment name> `

- Check revision history details  
`kubectl rollout history deployment <deployment name> --revision=1 `

- Revert back to revision 1  
`kubectl rollout undo deployment <deployment name> --to-revision=1 `

- Inspect deployments  
`kubectl describe deployments <deployment name> `
