Summary exercise
---------------
- Create user in [docker hub](https://hub.docker.com/)
- Create 2 multi stage images from nginx base and put static string in /index.html and echo Coffie string in index.html in coffie dir during the image create and Tea string in index.html in tea dir   
- Push both images to docker hub 
[Solution](https://gitlab.com/skywiz-io/kubernetes-workshop/-/tree/master/Summary/Docker)
- Create deployments with those images. Commands reminder in [deployments Task](https://gitlab.com/skywiz-io/kubernetes-workshop/-/blob/master/deployment/README.md)
- Create service for both deployments, assisstance [here](https://gitlab.com/skywiz-io/kubernetes-workshop/-/tree/master/services)
- create a deployment and service for the menu which will present links for both previously created applications
- Create volumes for the menu deployment to take the index.html from a [config maps](https://gitlab.com/skywiz-io/kubernetes-workshop/-/blob/master/configmap/Tasks)
- Create host based routing [ingress](https://gitlab.com/skywiz-io/kubernetes-workshop/-/blob/master/ingress/cheese-ingress.yaml) for those deployments
- Template the [yamls](https://gitlab.com/skywiz-io/kubernetes-workshop/-/tree/master/yaml) and create [helm](https://gitlab.com/skywiz-io/kubernetes-workshop/-/tree/master/helm) chart for the coffie tea application
- Deploy the helm chart
- Change the index.html contents in the chart, upgrade the release and check if works
