Docker create and push
----------------------
``` bash
mkdir coffie
cd coffie

# Create Docker file and index file from according directory
# build docker image
docker build -t <docker user>/coffie

#push to registry
docker push <docker user>/coffie

# The same for tea
```