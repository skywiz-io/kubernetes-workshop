Install Helm and run a sample application
-------------------------------------

``` bash
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

# View Helm Repositories
helm repo list 

# Check Helm Version 
helm version

# Add stable repository
helm repo add bitnami https://charts.bitnami.com/bitnami

# View Bitnami Helm Charts
helm search repo bitnami

# Install from repository
helm install my-release bitnami/mysql

# See all resources related to my-release
kubectl get all --selector=app.kubernetes.io/instance=my-release

# Secrets not shown in get all 
kubectl get secret --selector=app.kubernetes.io/instance=my-release

# See al using helm command
helm get manifest my-release

# Uninstall the demo
helm uninstall my-release

# View helm environment variables
helm env
```
