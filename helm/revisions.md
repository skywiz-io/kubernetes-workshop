Revisions lab
-------------
``` bash
# Create chart for tes purposes
helm create testchart

# Install the chart as release named demo
helm install demo testchart

# list all of Helm releases installed in your namespace
helm list

# Change `replicaCount: 1` to 3 in testchart/values.yaml
vim testchart/values.yaml

# upgrade to new Revisions
helm upgrade demo testchart

# see the pods to see the number increased
kubectl get pods

# check Revisions
helm history demo

# Revert to previous Revision
helm rollback demo 1

# Check number of pods
kubectl get pods

# Clean up
helm uninstall demo
```
