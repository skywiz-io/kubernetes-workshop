# Sample commands
```bash
source <(helm completion bash)
helm repo list
helm search repo nginx
helm show <option> <chartname>
all
chart
readme
values  
helm create testchart
helm inspect chart testchart
helm inspect readme testchart
```
