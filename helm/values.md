```bash
# View testchart values
helm show values testchart 

# Create a new values file with `replicaCount: 5`
vim my-testchart-values.yaml

# Install the chart as a release named demo and override chart's default values file
helm install demo testchart -f my-testchart-values.yaml

# list all of Helm releases installed in your namespace
helm list

# View pods and view naming format <release-name>-<chart-name>
kubectl get deploy,pods

# Add a new line in your values file: `fullnameOverride: <your-name>`
vim my-testchart-values.yaml

# Helm upgrade 'demo' release
helm upgrade demo testchart -f my-testchart-values.yaml

# View pods and view naming format <your-name>
kubectl get deploy,pods

# View Helm release name hasn't changed
helm list

# Clean up
helm uninstall demo
```
