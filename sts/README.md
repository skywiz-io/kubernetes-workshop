- Review the file sts.yaml - notice the  volumeClaimTemplates section and annotations  that refferences the storageClass
`cat sts.yaml`

- create Statefull Set
`kubectl create -f sts.yaml`

 - Review statefull set starting process notice the order of the pods and their names
``` bash
kubectl get pods -l app=nginx
kubectl get sts -o wide
kubectl describe sts web
```

- review the pv and pvc that were automatically creaded by the proccess
```bash
kubectl get pv
kubectl get pvc
```

- Create services for the statefull set notice the difference between them
``` bash
kubectl create -f ./service.yaml 
kubectl create -f ./service_sts.yaml
```

- Run and connect to busybox
`kubectl run busybox -it  --rm --image busybox`

- While inside of busybox, check connectivity to each service run twice to see the balancing between IPs in each case
``` bash
ping nginx-sts
ping nginx-sts
ping nginx-non-sts
ping nginx-non-sts
exit
```

 - Exit from container and delete sts and the services

