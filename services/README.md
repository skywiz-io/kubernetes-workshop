# Create 3 types of services:

## ClusterIP  
----------
- Run and expose deployment
`kubectl create deployment webapp --image=nginx`
`kubectl expose deployment webapp --type=ClusterIP --port=80 --target-port=80 `
- Get service info  
`kubectl get service `
- Check access to service in cluster  
`kubectl run busybox --image=busybox --rm -it --restart=Never -- wget -O- <Service ip>:80`  
`kubectl run busybox --image=busybox --rm -it --restart=Never -- wget -O- <Service-name>.<Your-name>.svc.cluster.local:80`
- Inspect service  
`kubectl describe svc <Service-name> `
- Delete service  
`kubectl delete svc <Service-name> `


## NodePort  
----------
- Expose deployment using a node port
`kubectl expose deployment webapp --type=NodePort --port=80 --target-port=80`
- Get node internal ip  
`kubectl get nodes -o wide`
- Get service node port  
`kubectl describe svc <Service-name> `
- check access to service via node port  
`kubectl run busybox --image=busybox --rm -it --restart=Never -- wget -O- <Node-internal-ip>:<Service-node-port>`
- Inspect service  
`kubectl describe svc <Service-name> `
- Delete service  
`kubectl delete svc <Service-name> `


## LoadBalancer
------------
- Exposed 
`kubectl expose deployment webapp --type=LoadBalancer --port=80 --target-port=80`
- Get service external loab balancer ip  
`kubectl get svc `
- Check access to service via load balancer  
    - Open Browser and put "http://\<External-load-balancer-ip\>"
- Inspect service  
`kubectl describe svc <Service-name> `
- Delete service  
`kubectl delete svc <Service-name> `

- Review the Yamls  
`kubectl get svc <Service-name> -o yaml`

