docker image ls
docker pull nginx
docker pull nginx:1.16
docker container ls
docker ps
docker container run busybox
docker container ls 
docker container ls -a
docker container ls -a -l --no-trunc -s
docker container run --name nginx-test -d nginx 
docker container inspect nginx-test
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' nginx-test

*curl <ip of docker>

docker container pause nginx-test

*curl http://<ip of docker>

docker container unpause nginx-test
docker container top nginx-test
docker container logs nginx-test
docker container stats nginx-test
docker container diff nginx-test

*docker container exec -it <id of container> <command> 

docker container exec -it nginx-test ls
docker container stop nginx-test
docker container ls
docker container ls -a
docker container prune 
docker container ls -a
docker container run nginx
docker container run -d  nginx
docker container run -d --rm  nginx
docker container run --rm -it  busybox
docker container ls
docker container ls -a
docker container run -d --name port-test -p 8080:80  nginx
netstat -ntlp
docker container run -d -p 8080:80 -p 8083:443 nginx
docker container port port-test
