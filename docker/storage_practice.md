- create volume called test-data
- run busybox container and mount test-data as /app
- create a file called lines in /app with 50 lines 
- run another busybox container and mount test-data as /app2
- count number of line in /app2/lines




```bash
docker volume create myvol
docker container run --name app1 -d -it --mount type=volume,source=test-data,target=/app busybox
docker container run  --name app2 -d -it --mount type=volume,source=test-data,target=/app2 busybox
docker exec -it  app1 sh
for i in $(seq 1 50) ;do echo "line number ${i}";done >> /app/lines
exit
docker exec -it   app2 sh
cat /app2/lines | wc -l 
```

```
