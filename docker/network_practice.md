- create localhost network
- run container with nginx in internal network 
- run busybox container in default network
- telnet  www.google.com 80
- telnet ip of nginx 80
- make busybox connect to nginx


  docker network create --internal localhost
  docker container run -d   --name local-nginx --network localhost nginx
  docker inspect local-nginx
  docker container run --name bridge-busybox -d --rm busybox  sleep 3600
  docker container exec -it  bridge-busybox sh
  telnet google.com 80
