Docker exercise
---------------


- create dockerfile
- create build stage with nginx as a base
- move index.html to the image in dockerfile (/usr/share/nginx/html/index.htm)
- in dockerfile execute a command to add more text to index.html
- create new stage nginx as a base
- copy index file from previous stage
- CMD ["nginx", "-g", "daemon off;"]
- run container and access the new index


``` bash
mkdir /tmp/ms
cd /tmp/ms

# create a new index.html file 
echo "this is my customer index.html" > ./index.html

# create dockerfile
touch Dockerfile

# create build stage with nginx as a base
echo "FROM nginx as build" >> Dockerfile

# move index.html to the image in dockerfile (/usr/share/nginx/html/index.htm)
echo "COPY index.html /usr/share/nginx/html/index.html" >> Dockerfile

# in dockerfile execute a command to add more text to index.html
echo 'RUN echo "here comes stuff that takes a lot of time and requires bcc and node and npm" >> /usr/share/nginx/html/index.html' >> Dockerfile

# create new stage nginx as a base
echo "FROM nginx" >> Dockerfile

# copy index file from previous stage
echo "COPY --from=build /usr/share/nginx/html/index.html /usr/share/nginx/html/index.html" >> Dockerfile

# Run nginx as entry point
echo 'CMD ["nginx","-g","daemon off;"]' >> Dockerfile

# Buid image from the docker file
docker build -t multi-stage .
docker run -d --name multi-stage  multi-stage
docker inspect multi-stage
curl http://<container_ip>

# push the docker to the repository
docker tag multi-stage:1.0 <registry>/multi-stage:1.0
docker push <registry>/multi-stage:1.0
```
