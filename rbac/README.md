- Create kubectl pod with no service account\
`kubectl create -f no-sa-pod.yaml`

<details>
<summary>Run kubectl get pods command from the pod</summary>
<p>
`kubectl exec -it kubectl-no-sa -- kubectl get pod`
</p>
</details>

- Create pod with Read-only service account, role, role binding
    ```
    kubectl create -f rosa.yaml
    kubectl create -f ROrole.yaml
    kubectl create -f RORB.yaml
    kubectl create -f ROpod.yaml
    ```
- Run get pod command\
`kubectl exec -it ropod -- kubectl get pod`

- Try to create pod\
`kubectl exec -it ropod -- kubectl run --generator=run-pod/v1 b -it --image=busybox`
- Try to delete a pod\
`kubectl exec -it ropod -- kubectl delete pod kubectl-no-sa`

- Create pod with Read-write service account, role, role binding
```
kubectl create -f rwsa.yaml
kubectl create -f RWrole.yaml
kubectl create -f RWRB.yaml
kubectl create -f RWpod.yaml
```

- Try to delete another pod from this one\
`kubectl exec -it rwpod -- kubectl delete pod kubectl-no-sa`
