# Installing and using Istio

This guide explains how to do a clean installation of Istio on a running GKE cluster.

## Before you begin

- Open a new Cloud Shell session and run the following to set environment variables for the zone and cluster name:
```bash
export CLUSTER_NAME="<CLUSTER_NAME>" # Enter the name of your GKE cluster
export CLUSTER_ZONE=us-central1-c # Change to the zone of your GKE cluster
```

- Configure kubectl command line access by running the following:
```bash
export GCLOUD_PROJECT=$(gcloud config get-value project)

gcloud container clusters get-credentials $CLUSTER_NAME \
    --zone $CLUSTER_ZONE --project $GCLOUD_PROJECT
```

- Grant admin permissions in the cluster to the current gcloud user:
```bash
kubectl create clusterrolebinding cluster-admin-binding \
    --clusterrole=cluster-admin \
    --user=$(gcloud config get-value core/account)
```

**Output** (do not copy)
```bash
clusterrolebinding.rbac.authorization.k8s.io/cluster-admin-binding created
```

## Installing Istio

- Download and extract the latest release:
```bash
export ISTIO_VERSION=1.7.4
curl -L https://istio.io/downloadIstio | sh -
```

- Move to the Istio package directory:
```bash
cd istio-${ISTIO_VERSION}
```

The installation directory contains:
- Sample applications in samples/
- The istioctl client binary in the bin/ directory.

- Add the istioctl client to your path:
```bash
export PATH=$PWD/bin:$PATH
```

- For this installation, we use the `demo` [configuration profile](https://istio.io/latest/docs/setup/additional-setup/config-profiles/). It’s selected to have a good set of defaults for testing, but there are other profiles for production or performance testing.
```bash
istioctl install --set profile=demo
```

**Output** (do not copy)
```bash
✔ Istio core installed
✔ Istiod installed
✔ Egress gateways installed
✔ Ingress gateways installed
✔ Installation complete
```

- Verify the installation
```bash
kubectl get pod -n istio-system
```

**Output** (do not copy)
```bash
NAME                                    READY   STATUS    RESTARTS   AGE
istio-egressgateway-64bd9c4cdb-xmlm8    1/1     Running   0          16s
istio-ingressgateway-5847bb7b6c-94p4j   1/1     Running   0          16s
istiod-67b4c76c6-5njt6                  1/1     Running   0          27s
```

## Injecting sidecar proxies

Istio Service Mesh uses sidecar proxies to enhance network security, reliability, and observability. With Istio Service Mesh, these functions are abstracted away from the application's primary container and implemented in a common out-of-process proxy delivered as a separate container in the same Pod.

Before you deploy workloads, configure sidecar proxy injection so that Istio Service Mesh can monitor and secure traffic.

Any workloads that were running on your cluster before you installed Istio Service Mesh need to have the sidecar proxy injected or updated so they have the current Istio Service Mesh version. Before you deploy new workloads, configure sidecar proxy injection so that Istio Service Mesh can monitor and secure traffic.

- Create a demo namespace:
```bash
kubectl create ns istio-demo
```

- Enable automatic sidecar injection to your namespace:
```bash
kubectl label namespace istio-demo istio-injection=enabled
```

## Deploy Bookinfo, an Istio-enabled multi-service application

In this task, you enable the `istioctl` tool, set up the **Bookinfo** sample microservices application, and explore the app.

### Download and configure istioctl

- Download and extract the Istio release, with the istioctl tool, and sample code:
```bash
export LAB_DIR=$HOME/bookinfo-lab
export ISTIO_VERSION=1.7.4

mkdir $LAB_DIR
cd $LAB_DIR

curl -L https://git.io/getLatestIstio | ISTIO_VERSION=$ISTIO_VERSION sh -
```

- Make the Istio tools visible to your environment, by adding `bin` to your **PATH**:
```bash
cd ./istio-*

export PATH=$PWD/bin:$PATH
```

The installation directory contains the following files which we'll use:
- Sample applications in `samples/`.
- The `istioctl` client binary in the `bin/` directory. Similar to `kubectl` for Kubernetes, this is the tool used to manage Istio, including network routing and security policies.

- Verify `istioctl` works:
```bash
istioctl version
```

**Output** (do not copy)
```bash
client version: 1.7.4
control plane version: 1.7.4
data plane version: 1.7.4 (2 proxies)
```

### Bookinfo Overview

Now that Istio is configured and verified, you can deploy one of the sample applications provided with the installation — [BookInfo](https://istio.io/docs/guides/bookinfo.html). This is a simple mock bookstore application made up of four microservices - all managed using Istio. Each microservice is written in a different language, to demonstrate how you can use Istio in a multi-language environment, without any changes to code.

The microservices are:

- **productpage:** calls the details and reviews microservices to populate the page.
- **details:** contains book information.
- **reviews:** contains book reviews. It also calls the ratings microservice.
- **ratings:** contains book ranking information that accompanies a book review.

There are 3 versions of the **reviews** microservice:

- Reviews **v1** doesn't call the ratings service.
- Reviews **v2** calls the ratings service and displays each rating as 1 - 5 black stars.
- Reviews **v3** calls the ratings service and displays each rating as 1 - 5 red stars.

The end-to-end architecture of the application looks like this:

<img src="img/bookinfo.png" width="500"/>

You can find the source code and all the other files used in this example in the Istio [samples/bookinfo](https://github.com/istio/istio/tree/master/samples/bookinfo) directory.

### Deploy Bookinfo

- Look at the `.yaml` which describes the bookInfo application:
```bash
cat samples/bookinfo/platform/kube/bookinfo.yaml
```
Look for `containers` to see that each Deployment, has **one** container, for each version of each service in the Bookinfo application.

- Look at the same `.yaml` with Istio proxy sidecars *injected* using *istioctl*:
```bash
istioctl kube-inject -f samples/bookinfo/platform/kube/bookinfo.yaml
```
Now, when you scroll up, and look for `containers`, you can see extra configuration describing the proxy sidecar containers that will be deployed.

> `istioctl kube-inject` takes a Kubernetes YAML file as input, and outputs a version of that YAML which includes the Istio proxy. You can look at one of the Deployments in the output from `istio kube-inject`. It includes a second container, the **Istio sidecar**, along with all the configuration necessary.

- Use the following command to inject the proxy sidecar along with each application Pod that is deployed.
```bash
kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml -n istio-demo
```

> Istio uses an extended version of the open-source [Envoy proxy](https://www.envoyproxy.io/envoy/), a high-performance proxy developed in C++, to mediate all inbound and outbound traffic for all services in the service mesh. Istio leverages Envoy's many built-in features including dynamic service discovery, load balancing, TLS termination, HTTP/2 & gRPC proxying, circuit breakers, health checks, staged rollouts with %-based traffic split, fault injection, and rich metrics.

**Output** (do not copy)
```bash
service/details created
serviceaccount/bookinfo-details created
deployment.apps/details-v1 created
service/ratings created
serviceaccount/bookinfo-ratings created
deployment.apps/ratings-v1 created
service/reviews created
serviceaccount/bookinfo-reviews created
deployment.apps/reviews-v1 created
deployment.apps/reviews-v2 created
deployment.apps/reviews-v3 created
service/productpage created
serviceaccount/bookinfo-productpage created
deployment.apps/productpage-v1 created
```

### Enable external access using an Istio Ingress Gateway

- Look at the `.yaml` which describes the configuration for the application ingress gateway.
```bash
cat samples/bookinfo/networking/bookinfo-gateway.yaml
```
Look for the `Gateway` and `VirtualService` mesh resources which get deployed. The `Gateway` exposes services to users outside the service mesh, and allows Istio features such as monitoring and route rules to be applied to traffic entering the cluster.

- Configure the ingress gateway for the application, which exposes an external IP you will use later
```bash
kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml -n istio-demo
```

**Output** (do not copy)
```bash
gateway.networking.istio.io/bookinfo-gateway created
virtualservice.networking.istio.io/bookinfo created
```

### Verify the Bookinfo deployments

- Review services
```bash
kubectl get services -n istio-demo
```

**Output** (do not copy)
```bash
NAME          TYPE        ...
details       ClusterIP   ...
kubernetes    ClusterIP   ...
productpage   ClusterIP   ...
ratings       ClusterIP   ...
reviews       ClusterIP   ...
```

- Review running application pods
```bash
kubectl get pods -n istio-demo
```

**Output** (do not copy)
```bash
NAME                              READY     STATUS
details-v1-1520924117-48z17       2/2       Running
productpage-v1-560495357-jk1lz    2/2       Running
ratings-v1-734492171-rnr5l        2/2       Running
reviews-v1-874083890-f0qf0        2/2       Running
reviews-v2-1343845940-b34q5       2/2       Running
reviews-v3-1813607990-8ch52       2/2       Running
```

- Confirm that the Bookinfo application is running by sending a curl request to it from some Pod, within the cluster, for example from `ratings`:
```bash
kubectl -n istio-demo exec -it $(kubectl -n istio-demo get pod -l app=ratings \
    -o jsonpath='{.items[0].metadata.name}') \
    -c ratings -- curl productpage:9080/productpage | grep -o "<title>.*</title>"
```

**Output** (do not copy)
```html
<title>Simple Bookstore App</title>
```

- Confirm the ingress gateway has been created
```bash
kubectl get gateway -n istio-demo
```

**Output** (do not copy)
```bash
NAME               AGE
bookinfo-gateway   20m
```

- Get the external IP address of the ingress gateway
```bash
kubectl get svc istio-ingressgateway -n istio-system
```

**Output** (do not copy)
```
NAME                   TYPE           CLUSTER-IP    EXTERNAL-IP        
istio-ingressgateway   LoadBalancer   10.97.2.253   <IngressGateway_IP>
```

- Now run the following command, replacing [EXTERNAL-IP] with the external IP that was outputted from the previous command
```bash
export GATEWAY_URL=[EXTERNAL-IP]
```

- Check that the Bookinfo app is running by sending a curl request to it from outside the cluster
```bash
curl -I http://${GATEWAY_URL}/productpage
```

**Output** (do not copy)
```bash
HTTP/1.1 200 OK
content-type: text/html; charset=utf-8
content-length: 4183
server: istio-envoy
...
```

## Try the application in your Web browser

- Point your browser to http://[GATEWAY_URL]/productpage to see the BookInfo web page. Don't forget to replace `[GATEWAY_URL]` with your working external IP address. 

![productpage](img/productpage.png)

- Refresh the page several times.

Notice how you see three different versions of reviews, since we have not yet used Istio to control the version routing.

There are three different book review services being called in a round-robin style:

- no stars
- black stars
- red stars

Switching among the three is normal Kubernetes routing/balancing behavior.

## Visualizing Your Mesh

This task shows you how to visualize different aspects of your Istio mesh.

- Install Kiali and wait for it to be deployed
```bash
kubectl apply -f samples/addons
while ! kubectl wait --for=condition=available --timeout=600s deployment/kiali -n istio-system; do sleep 1; done
```

> If there are errors trying to install the addons, try running the command again. There may be some timing issues which will be resolved when the command is run again.

- Apply the following configuration to expose Kiali
```bash
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: kiali-gateway
  namespace: istio-system
spec:
  selector:
    istio: ingressgateway
  servers:
  - port:
      number: 80
      name: http-kiali
      protocol: HTTP
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: kiali-vs
  namespace: istio-system
spec:
  hosts:
  - "*"
  gateways:
  - kiali-gateway
  http:
  - route:
    - destination:
        host: kiali
        port:
          number: 20001
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: kiali
  namespace: istio-system
spec:
  host: kiali
  trafficPolicy:
    tls:
      mode: DISABLE
---
EOF
```

**Output** (do not copy)
```bash
gateway.networking.istio.io/kiali-gateway created
virtualservice.networking.istio.io/kiali-vs created
destinationrule.networking.istio.io/kiali created
```

- Create Kiali credentials secret
```bash
kubectl create secret generic kiali -n istio-system --from-literal=username=admin --from-literal=passphrase=admin
```

- Send traffic to the mesh
```
watch -n 1 curl -o /dev/null -s -w %{http_code} $GATEWAY_URL/productpage
```

- Point your browser to http://[GATEWAY_URL]/kiali to see the Kiali web page.

![kiali_login](img/kiali_login.png)

- Login using `admin` as username and password

- Once logged in, click on graph icon to view a Bookinfo application graph.

![kiali_show_graph](img/kiali_show_graph.png)

![kiali_bookinfo_graph](img/kiali_bookinfo_graph.png)

- To view a summary of metrics, select any node or edge in the graph to display its metric details in the summary details panel on the right.

- To view your service mesh using different graph types, select a graph type from the **Graph Type** drop down menu. There are several graph types to choose from: **App**, **Versioned App**, **Workload**, **Service**.

- To view traffic animation, click on **Display** and choose **Traffic Animation** 

![kiali_bookinfo_graph_anim](img/kiali_bookinfo_graph_anim.png)

## Clean Up

### Delete BookInfo application

- Run the cleanup and replace put your namespace name when requested:
```bash
samples/bookinfo/platform/kube/cleanup.sh
```

> Choose the namespace your provisioned your application in (`istio-demo` in this example)

**Output** (do not copy)
```bash
virtualservice.networking.istio.io "bookinfo" deleted
gateway.networking.istio.io "bookinfo-gateway" deleted
Application cleanup may take up to one minute
service "details" deleted
serviceaccount "bookinfo-details" deleted
deployment.apps "details-v1" deleted
service "ratings" deleted
serviceaccount "bookinfo-ratings" deleted
deployment.apps "ratings-v1" deleted
service "reviews" deleted
serviceaccount "bookinfo-reviews" deleted
deployment.apps "reviews-v1" deleted
deployment.apps "reviews-v2" deleted
deployment.apps "reviews-v3" deleted
service "productpage" deleted
serviceaccount "bookinfo-productpage" deleted
deployment.apps "productpage-v1" deleted
Application cleanup successful
```

- Delete the `istio-demo` namespace
```bash
kubectl delete namespace istio-demo
```

### Uninstalling Anthos Service Mesh

- Remove the control plane
```bash
kubectl delete -f samples/addons
istioctl manifest generate --set profile=demo | kubectl delete --ignore-not-found=true -f -
```

- Delete the `istio-system` namespace
```bash
kubectl delete namespace istio-system
```

# Conclusion
Congratulations on successfully completing this hands-on lab!
