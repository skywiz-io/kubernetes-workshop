- Suppose your application is a REST API server, marked with labels app=bookstore and role=api:  
`kubectl run apiserver --image=nginx --labels app=bookstore,role=api --expose --port 80`

- Save the following NetworkPolicy to api-allow.yaml to restrict the access only to other pods (e.g. other microservices) running with label app=bookstore:

``` yaml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: api-allow
spec:
  podSelector:
    matchLabels:
      app: bookstore
      role: api
  ingress:
  - from:
      - podSelector:
          matchLabels:
            app: bookstore
```

`kubectl apply -f api-allow.yaml`

- Test the Network Policy is blocking the traffic, by running a Pod without the app=bookstore label:
``` bash
kubectl run test-$RANDOM --rm -i -t --image=alpine  -- sh
/ # wget -qO- --timeout=2 http://apiserver
wget: download timed out
```

- Traffic is blocked!


- Test the Network Policy is allowing the traffic, by running a Pod with the app=bookstore label:
``` bash
kubectl run test-$RANDOM --rm -i -t --image=alpine  --labels app=bookstore,role=frontend -- sh
/ # wget -qO- --timeout=2 http://<POD IP>
```
- Traffic is allowed.
