- DENY all non-whitelisted traffic to a namespace (put your machine name as namespace)

``` yaml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: default-deny-all
  namespace: <namespace>
spec:
  podSelector: {}
  ingress: []
```
`kubectl apply -f default-deny-all.yaml`

- Run a web server and expose it to the internet with a Load Balancer:

``` bash
kubectl run --generator=run-pod/v1 web --image=nginx \
    --labels=app=web --port 80

kubectl expose pod/web --type=LoadBalancer
```

- Wait until an EXTERNAL-IP appears on kubectl get service output. Visit the http://[EXTERNAL-IP] on your browser and verify it is accessible.

- The following manifest allows traffic from all sources (both internal from the cluster and external). Save it to web-allow-external.yaml and apply to the cluster:
``` yaml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: web-allow-external
spec:
  podSelector:
    matchLabels:
      app: web
  ingress:
  - from: []
```

`kubectl apply -f web-allow-external.yaml`
networkpolicy "web-allow-external" created
- Visit the http://[EXTERNAL-IP] on your browser again and verify it still works.