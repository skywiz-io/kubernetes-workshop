- Run a nginx Pod with labels app=web and expose it at port 80:
`kubectl run web --image=nginx --labels app=web --expose --port 80`

- Run a temporary Pod and make a request to web Service:  
``` bash
kubectl run --rm -i -t --image=alpine test-<NAME>  -- sh
/ # wget -qO- --timeout=2 http://<POD-IP>
```

- It works, now save the following manifest to web-deny-all.yaml, then apply to the cluster:
``` yaml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: web-deny-all
spec:
  podSelector:
    matchLabels:
      app: web
  ingress: []
```

- And create:
`kubectl apply -f web-deny-all.yaml `

- Run a test container again, and try to query web:
`kubectl run --rm -i -t --image=alpine test-<NAME>  -- sh`
`/ # wget -qO- --timeout=2 http://<POD-IP>`
wget: download timed out

- The traffic should be dropped.


* POD IP could be gotten with:
kubectl get pods  -o wide
